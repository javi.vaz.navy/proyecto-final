import { useState } from "react";
import ReactQuill from "react-quill";
import { Redirect } from "react-router";
import "react-quill/dist/quill.snow.css";
import "./EditProfile.css";
import { useUser } from "./UserContext";

function EditProfile() {
  const user = useUser();
  const [bio, setBio] = useState();
  const [avatar, setAvatar] = useState();

  const hanldeSubmit = async (e) => {
    e.preventDefault();

    const resBio = await fetch(
      `${process.env.REACT_APP_BACKEND}/api/users/${user.id}`,
      {
        method: "PATCH",
        body: JSON.stringify({ bio }),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + user.token,
        },
      }
    );

    if (avatar) {
      await fetch(
        `${process.env.REACT_APP_BACKEND}/api/users/${user.id}/avatar`,
        {
          method: "PATCH",
          body: JSON.stringify({ avatar }),
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + user.token,
          },
        }
      );
    }

    if (resBio.ok) return <Redirect to="/" />;
  };

  return (
    <div className="edit-profile">
      <form onSubmit={hanldeSubmit}>
        <label className="info-bio">
          <p className="biografia">Biografía</p>
          <ReactQuill className="bio" onChange={setBio} />
        </label>
        <label className="add-avatar">
          <p className="avatar">Avatar</p>
          <input
            className="input-avatar"
            type="file"
            onChange={(e) => setAvatar(e.target.value)}
          />
        </label>
        <div className="button">
          <button className="button-button">Confirmar cambios</button>
        </div>
      </form>
    </div>
  );
}

export default EditProfile;
