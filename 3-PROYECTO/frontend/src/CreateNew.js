import { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { useUser } from "./UserContext";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "./CreateNew.css";
import "./CardNew.css";

function CreateNew() {
  const [title, setTitle] = useState("");
  const [category, setCategory] = useState("");
  const [text, setText] = useState("");
  const [image, setImage] = useState();
  const [part, setPart] = useState(1);
  const user = useUser();

  const history = useHistory();

  const clickPart = (e) => {
    e.preventDefault();
    setPart(2);
  };
  const prevPart = (e) => {
    e.preventDefault();
    setPart(1);
  };

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();

      const res = await fetch(`${process.env.REACT_APP_BACKEND}/api/news`, {
        method: "POST",
        body: JSON.stringify({ title, category, text }),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + user.token,
        },
      });

      const [notice] = await res.json();

      if (image) {
        const payload = new FormData();
        payload.append("image", image);

        await fetch(
          `${process.env.REACT_APP_BACKEND}/api/news/${notice.id}/image`,
          {
            method: "PATCH",
            body: payload,
            headers: {
              Authorization: "Bearer " + user.token,
            },
          }
        );
      }

      history.push(`/news/${notice.id}`);
    } catch (error) {
      console.error(error);
    }
  };

  if (!user) {
    return <Redirect to="/login" />;
  }

  return (
    <div className="new-new">
      <div className="create-new">
        <div className="new-content">
          <h1>Create a New</h1>
          <form>
            {part === 1 && (
              <div className="category">
                <p className="category-p">Please choose one category:</p>
                <div className="category-radio">
                  <div className="category-music">
                    <input
                      type="radio"
                      id="Festivales Nacionales"
                      name="category"
                      value="1"
                      onChange={(e) => setCategory(e.target.value)}
                    />
                    <label for="Music">Festivales Nacionales</label>
                  </div>
                  <div className="category-topicality">
                    <input
                      type="radio"
                      id="Festivales Europeos"
                      name="category"
                      value="2"
                      onChange={(e) => setCategory(e.target.value)}
                    />
                    <label for="Topicality">Festivales Europeos</label>
                  </div>
                  <div className="category-general">
                    <input
                      type="radio"
                      id="Conciertos"
                      name="category"
                      value="3"
                      onChange={(e) => setCategory(e.target.value)}
                    />
                    <label for="General">Conciertos</label>
                  </div>
                  <div className="category-computing">
                    <input
                      type="radio"
                      id="Discotecas"
                      name="category"
                      value="4"
                      onChange={(e) => setCategory(e.target.value)}
                    />
                    <label for="Computing">Discotecas</label>
                  </div>
                  <div className="category-programming">
                    <input
                      type="radio"
                      id="Programming"
                      name="category"
                      value="5"
                      onChange={(e) => setCategory(e.target.value)}
                    />
                    <label for="Programming">Discotecas Europeas</label>
                  </div>
                </div>
                <div className="div-button">
                  <button className="button" onClick={clickPart}>
                    Continue
                  </button>
                </div>
              </div>
            )}

            {part === 2 && (
              <div className="notice">
                <label className="label">
                  <input
                    className="title"
                    name="title"
                    placeholder="Notice title..."
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </label>
                <label className="label">
                  <ReactQuill
                    className="text"
                    name="text"
                    placeholder="Notice text..."
                    value={text}
                    onChange={setText}
                  />
                </label>
                <label className="label">
                  <input
                    className="input-image"
                    type="file"
                    onChange={(e) => setImage(e.target.files[0])}
                  />
                </label>
                <div className="buttons">
                  <button className="previously" onClick={prevPart}>
                    Back
                  </button>
                  <button onClick={handleSubmit} className="create-new">
                    Create New
                  </button>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateNew;
