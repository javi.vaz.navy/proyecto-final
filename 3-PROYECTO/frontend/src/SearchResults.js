import { Link } from "react-router-dom";
import queryString from "query-string";
import useFetch from "./useFetch";

function SearchResults({ q, searchOpts }) {
  const [results] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/news?` +
      queryString.stringify({ title: q, ...searchOpts })
  );
  console.log(results);

  if (!results) return <div>Buscando...</div>;

  return (
    <div className="results">
      <h2>Resultados:</h2>
      {results.length > 0 ? (
        results.map((e) => (
          <li className="result" key={e.id}>
            <Link className="link-result" to={`/news/${e.id}`}>
              {e.title}
            </Link>
          </li>
        ))
      ) : (
        <p>No hay resultado</p>
      )}
    </div>
  );
}

export default SearchResults;
