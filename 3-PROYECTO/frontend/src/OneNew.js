import useFetch from "./useFetch";
import { useUser } from "./UserContext";
import { Link, useParams } from "react-router-dom";
import "./OneNew.css";

function OneNew() {
  const user = useUser();
  const { id: newId } = useParams();

  const [notice, setNotice] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/news/${newId}/comment`
  );
  const [categories] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/categories`
  );

  if (!notice || !categories) return <p className="cargando">Cargando...</p>;

  const deleteComment = async (id) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}/api/news/${newId}/comment/${id}`,
        {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + user.token,
          },
        }
      );

      const data = await res.json();

      if (res.ok) {
        setNotice(data);
      } else {
        throw new Error("Error borrando la noticia");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const voteNew = async (id, value) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}/api/news/${id}/valoration`,
        {
          method: "POST",
          body: JSON.stringify({ value }),
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + user.token,
          },
        }
      );

      const data = await res.json();

      if (res.ok) {
        setNotice(data);
      } else {
        throw new Error("Error votando noticia");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="one-new">
      <div className="new">
        <div className="new-header">
          <h2 className="title">{notice.title}</h2>

          {categories
            .filter((u) => u.id === notice.categorie_id)
            .map((us) => (
              <div key={notice.id} className="categorie-info">
                <p className="categorie">{us.categorie_name}</p>
              </div>
            ))}

          {user?.id === notice.user_id && (
            <div className="edit-new">
              <Link to={`/news/${notice.id}/edit`} className="edit-link">
                Edit
              </Link>
            </div>
          )}

          <div className="user-info">
            <p className="username">{notice.user_name}</p>
            {notice.avatar ? (
              <img
                className="avatar"
                src={`${process.env.REACT_APP_BACKEND}/images/${notice.avatar}`}
                alt="avatar"
              />
            ) : (
              <img
                className="without-avatar"
                src={`${process.env.REACT_APP_BACKEND}/images/userOption1.png`}
                alt="avatar"
              />
            )}
          </div>
        </div>
        <div className="new-notice">
          <div
            className="text"
            dangerouslySetInnerHTML={{ __html: notice.text }}
          />
          <div className="div-image">
            <img
              className="image"
              src={`${process.env.REACT_APP_BACKEND}/images/${notice.image}`}
              alt="imágen de la noticia"
            />
          </div>
        </div>

        <div className="footer">
          <div className="comment">
            <Link className="comment-link" to={`/news/${newId}/comment`}>
              Comentar
            </Link>
          </div>
          {user && (
            <div className="valoration">
              <div className="vote-positive">
                <button
                  onClick={(e) => {
                    e.preventDefault();
                    voteNew(notice.id, 1);
                  }}
                  className="emoji"
                >
                  👍
                </button>
                <p className="count">{notice.positiveVotes}</p>
              </div>
              <div className="vote-negative">
                <button
                  onClick={(e) => {
                    e.preventDefault();
                    voteNew(notice.id, -1);
                  }}
                  className="emoji"
                >
                  👎
                </button>
                <p className="count">{notice.negativeVotes}</p>
              </div>
            </div>
          )}
          {!user && (
            <div className="valoration">
              <div className="vote-positive">
                <p className="emoji-nobutton">👍</p>
                <p className="count">{notice.positiveVotes}</p>
              </div>
              <div className="vote-negative">
                <p className="emoji-nobutton">👎</p>
                <p className="count">{notice.negativeVotes}</p>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className="comments">
        {notice.comments?.map((c) => (
          <div key={c.id} className="comment">
            <div className="comment-info">
              <div className="user-info">
                <img
                  className="avatar"
                  src={`${process.env.REACT_APP_BACKEND}/images/${notice.avatar}`}
                  alt="avatar"
                />
                <p className="username">{c.username_cmnt}</p>
              </div>

              {user?.id === notice.user_id && (
                <button
                  className="delete-comment"
                  onClick={(e) => {
                    e.preventDefault();
                    deleteComment(c.id);
                  }}
                >
                  Delete
                </button>
              )}
            </div>
            <div className="content-cmoment">
              <p>{c.text}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default OneNew;
