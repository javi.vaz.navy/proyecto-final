import { Switch, Route } from "react-router-dom";
import Helmet from "react-helmet";
import "./App.css";
import "./CardNew.css";
import Menu from "./Menu";
import Search from "./Search";
import Login from "./Login";
import Profile from "./Profile";
import Home from "./Home";
import Signin from "./Signin";
import EditNew from "./EditNew";
import CreateNew from "./CreateNew";
import CardNew from "./CardNew";
import CommentNew from "./CommentNew";
import OneNew from "./OneNew";

function App() {
  return (
    <div className="App">
      <Menu />
      <Helmet>
        <title>YourFavFests</title>
      </Helmet>
      <main>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/search/:q?" exact>
            <Search />
          </Route>
          <Route path="/new/create" exact>
            <CreateNew />
          </Route>
          <Route path="/login" exact>
            <Login />
          </Route>
          <Route path="/signin" exact>
            <Signin />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
          <Route path="/news/:id" children={OneNew} exact>
            <OneNew />
          </Route>
          <Route path="/news/:id/comment" children={CommentNew} exact>
            <CommentNew />
          </Route>
          <Route path="/news/:id/edit" children={EditNew} exact>
            <EditNew />
          </Route>
          <Route path="/new" exact>
            <CardNew />
          </Route>
          <Route path="/">Not Found</Route>
        </Switch>
      </main>
    </div>
  );
}

export default App;
