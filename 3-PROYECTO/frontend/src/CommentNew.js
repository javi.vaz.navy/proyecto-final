import { useUser } from "./UserContext";
import { useState } from "react";
import { Redirect, useParams, useHistory } from "react-router";
import "./CommentNew.css";

function CommentNew() {
  const user = useUser();
  const { id } = useParams();
  const [text, setText] = useState();
  const [error, setError] = useState(null);

  const history = useHistory();

  if (!user) return <Redirect to="/login" />;

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}/api/news/${id}/comment`,
        {
          method: "POST",
          body: JSON.stringify({ text }),
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + user.token,
          },
        }
      );

      const data = res.json();

      if (res.ok) {
        history.push(`/news/${id}`);
      } else {
        setError(data.error);
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="comment-new">
      <form className="form-comment">
        <h1>Create a comment</h1>
        <div className="div-comment">
          {error && <div className="error">{error + "!!"}</div>}
          <label className="label">
            <textarea
              className="comment"
              name="comment"
              placeholder="Comment notice..."
              value={text}
              onChange={(e) => setText(e.target.value)}
            />
          </label>
        </div>
        <div className="div-button">
          <button className="button" onClick={handleSubmit}>
            Comment
          </button>
        </div>
      </form>
    </div>
  );
}

export default CommentNew;
