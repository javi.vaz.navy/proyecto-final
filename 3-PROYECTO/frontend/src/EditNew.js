import { useUser } from "./UserContext";
import { useState } from "react";
import { Redirect, useParams } from "react-router";
import ReactQuill from "react-quill";
import "./EditNew.css";
import useFetch from "./useFetch";

function EditNew() {
  const user = useUser();
  const { id } = useParams();

  const [lastNotice] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/news/${id}/comment`
  );

  const [text, setText] = useState();
  const [title, setTitle] = useState();
  const [image, setImage] = useState();
  const [categoria, setCategory] = useState();
  const [part, setPart] = useState(1);

  const clickPart = (e) => {
    e.preventDefault();
    setPart(2);
  };
  const prevPart = (e) => {
    e.preventDefault();
    setPart(1);
  };

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();

      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}/api/news/${id}`,
        {
          method: "PATCH",
          body: JSON.stringify({ title, categoria, text }),
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + user.token,
          },
        }
      );

      if (image) {
        const payload = new FormData();
        payload.append("image", image);

        await fetch(`${process.env.REACT_APP_BACKEND}/api/news/${id}/image`, {
          method: "PATCH",
          body: payload,
          headers: {
            Authorization: "Bearer " + user.token,
          },
        });

        if (image) {
          return <Redirect to={`/news/${id}`} />;
        }
      }

      if (res.ok) {
        return <Redirect to={`/news/${id}`} />;
      }
    } catch (error) {
      console.error(error);
    }
  };

  if (!lastNotice) return <div>buscando...</div>;
  return (
    <div className="edit-new">
      <div className="edit-content">
        <h1>Edit your new</h1>
        <form>
          {part === 1 && (
            <div className="category">
              <p className="category-p">Please choose one category:</p>
              <div className="category-radio">
                <div className="category-music">
                  <input
                    type="radio"
                    id="Festivales Nacionales"
                    name="selection"
                    value="1"
                    onClick={(e) => setCategory(e.target.value)}
                  />
                  <label for="Music">Festivales Nacionales</label>
                </div>
                <div className="category-topicality">
                  <input
                    type="radio"
                    id="Festivales Europeos"
                    name="selection"
                    value="2"
                    onClick={(e) => setCategory(e.target.value)}
                  />
                  <label for="Topicality">Festivales Europeos</label>
                </div>
                <div className="category-general">
                  <input
                    type="radio"
                    id="Conciertos"
                    name="selection"
                    value="3"
                    onClick={(e) => setCategory(e.target.value)}
                  />
                  <label for="General">Conciertos</label>
                </div>
                <div className="category-computing">
                  <input
                    type="radio"
                    id="Discotecas"
                    name="selection"
                    value="4"
                    onClick={(e) => setCategory(e.target.value)}
                  />
                  <label for="Computing">Discotecas</label>
                </div>
                <div className="category-programming">
                  <input
                    type="radio"
                    id="Programming"
                    name="selection"
                    value="5"
                    onClick={(e) => setCategory(e.target.value)}
                  />
                  <label for="Programming">Discotecas Europeas</label>
                </div>
              </div>
              <div className="div-button">
                <button className="button" onClick={clickPart}>
                  Continue
                </button>
              </div>
            </div>
          )}

          {part === 2 && (
            <div className="notice">
              <label className="label">
                <input
                  defaultValue={lastNotice.title}
                  className="title"
                  name="title"
                  placeholder="Notice title..."
                  value={title}
                  onSubmit={(e) => setTitle(e.target.value)}
                />
              </label>
              <label className="label">
                <ReactQuill
                  defaultValue={lastNotice.text}
                  className="text"
                  name="text"
                  onChange={setText}
                />
              </label>
              <label className="label">
                <input
                  className="input-image"
                  type="file"
                  onChange={(e) => setImage(e.target.files[0])}
                />
              </label>
              <div className="buttons">
                <button className="previously" onClick={prevPart}>
                  Back
                </button>
                <button onClick={handleSubmit} className="create-new">
                  Create New
                </button>
              </div>
            </div>
          )}
        </form>
      </div>
    </div>
  );
}

export default EditNew;
