import { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";
import queryString from "query-string";
import SearchResults from "./SearchResults";
import "./Search.css";

function Search() {
  const { q } = useParams();
  const loc = useLocation();
  const searchOpts = queryString.parse(loc.search);

  const history = useHistory();

  const [search, setSearch] = useState(q || "");
  const [categorie_id, setCategory] = useState(searchOpts.categorie_id || "");
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    const options = { categorie_id };
    history.push("/search/" + search + "?" + queryString.stringify(options));
    dispatch({ type: "SEARCH", search });
  };

  return (
    <div className="search">
      <h1>Buscador de noticias</h1>
      <form className="form" onSubmit={handleSubmit}>
        <input
          className="search-input"
          placeholder="Título..."
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <select
          className="category"
          value={categorie_id}
          onChange={(e) => setCategory(e.target.value)}
        >
          <option value="">todas</option>
          <option value={1}>Festivales Nacionales</option>
          <option value={2}>Festivales Europeos</option>
          <option value={3}>Conciertos</option>
          <option value={4}>Discotecas</option>
          <option value={5}>Discotecas Europeas</option>
        </select>
        <button className="button">Search</button>
      </form>
      {q && <SearchResults q={q} searchOpts={searchOpts} />}
    </div>
  );
}

export default Search;
