import { NavLink, Redirect, Route, Switch } from "react-router-dom";
import { useUser } from "./UserContext";
import useFetch from "./useFetch";
import "./Profile.css";
import "./ProfileCardNew.css";
import ProfileCardNew from "./ProfileCardNew";
import MyProfile from "./MyProfile";
import EditProfile from "./EditProfile";

function Profile() {
  const user = useUser();

  const [notices, setNotices] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/news`
  );
  const [categories] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/categories`
  );

  if (!user) return <Redirect to="/login" />;

  return (
    <div className="profile">
      <div className="box">
        <div className="tabs">
          <NavLink to="/profile/news" exact activeClassName="active">
            My news
          </NavLink>
          <NavLink to="/profile" exact activeClassName="active">
            My profile
          </NavLink>
        </div>
        <div className="content">
          <Switch>
            <Route path="/profile/news" exact>
              {notices
                ?.filter((n) => user.id === n.user_id)
                .map((us) => (
                  <ProfileCardNew
                    key={us.id}
                    notice={us}
                    setNotices={setNotices}
                    categories={categories}
                    user={user}
                  />
                ))}
            </Route>
            <Route path="/profile" exact>
              <MyProfile />
            </Route>
            <Route path="/profile/edit" exact>
              <EditProfile />
            </Route>
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default Profile;
