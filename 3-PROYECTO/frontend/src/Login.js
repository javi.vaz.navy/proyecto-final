import { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import { useSetUser, useUser } from "./UserContext";
import "./Login.css";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const user = useUser();
  const setUser = useSetUser();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3100/api/users/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, password }),
    });

    const data = await res.json();
    if (res.ok) {
      setUser(data);
    } else {
      setError(data.error);
    }
  };

  if (user) {
    return <Redirect to="/" />;
  }

  return (
    <div className="login">
      <div className="log-main">
        <h1>Login</h1>
        <form className="log-form" onSubmit={handleSubmit}>
          {error && <div className="error">{error + "!!"}</div>}
          <label className="label">
            <input
              className="email"
              name="email"
              placeholder="Email..."
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label className="label">
            <input
              className="password"
              name="password"
              type="password"
              placeholder="Password..."
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <button>Log in</button>
        </form>
        <p>
          <span>Aún no tienes cuenta?</span>
          <Link className="link" to="/signin">
            Regístrate
          </Link>
        </p>
      </div>
    </div>
  );
}

export default Login;
