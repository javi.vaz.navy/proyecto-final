import { NavLink, Link } from "react-router-dom";
import "./Menu.css";
import { useUser } from "./UserContext";

function Menu() {
  const user = useUser();

  return (
    <aside className="menu">
      <div className="top">
        <NavLink to="/" activeClassName="active" exact>
          Home
        </NavLink>
        <NavLink to="/search" activeClassName="active" exact>
          Search
        </NavLink>
      </div>
      <div className="middle">
        <h1 className="title">YourFavFests</h1>
      </div>
      <div className="bottom">
        {user && (
          <Link className="create-new" to="/new/create">
            Create New
          </Link>
        )}
        {!user && <Link to="/login">Log in</Link>}
        {user && (
          <Link className="user-info" to="/profile">
            {/* <div
              className="avatar"
              style={{ backgroundImage: `url(${user.avatar})` }}
            /> */}

            {user.avatar ? (
              <img
                className="avatar"
                src={`${process.env.REACT_APP_BACKEND}/images/${user.avatar}`}
                alt="avatar"
              />
            ) : (
              <img
                className="without-avatar"
                src={`${process.env.REACT_APP_BACKEND}/images/userOption1.png`}
                alt="avatar"
              />
            )}

            <span className="username">{user.userName}</span>
          </Link>
        )}
      </div>
    </aside>
  );
}

export default Menu;
