import { Link } from "react-router-dom";

function CardNew({ notice, categories, user, setNotices }) {
  const deleteNew = async (id) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}/api/news/${notice.id}`,
        {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + user.token,
          },
        }
      );

      const data = await res.json();

      if (res.ok) {
        setNotices(data);
      } else {
        throw new Error("Error borrando la noticia");
      }
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div className="new">
      <Link className="link" to={`/news/${notice.id}`}>
        <div className="new-header">
          <h2 className="title">{notice.title}</h2>

          {categories
            .filter((u) => u.id === notice.categorie_id)
            .map((us) => (
              <div key={notice.id} className="categorie-info">
                <p className="categorie">{us.categorie_name}</p>
              </div>
            ))}

          <div className="user-info">
            <p className="username">{notice.user_name}</p>

            {notice.avatar ? (
              <img
                className="avatar"
                src={`${process.env.REACT_APP_BACKEND}/images/${notice.avatar}`}
                alt="avatar"
              />
            ) : (
              <img
                className="without-avatar"
                src={`${process.env.REACT_APP_BACKEND}/images/userOption1.png`}
                alt="avatar"
              />
            )}
          </div>
        </div>
        <div className="new-notice">
          <div
            className="text"
            dangerouslySetInnerHTML={{ __html: notice.text }}
          />
          <div className="div-image">
            {notice.image && (
              <img
                className="image"
                src={`${process.env.REACT_APP_BACKEND}/images/${notice.image}`}
                alt="imágen de la noticia"
              />
            )}
          </div>
        </div>
      </Link>

      <div className="footer">
        <div className="comments">
          {notice.nComments ? (
            <div className="con-comentarios">
              {notice.nComments} Comentario(s)
            </div>
          ) : (
            <div className="sin-comentarios">Sin comentarios</div>
          )}
        </div>

        {
          <div className="valoration">
            <div className="vote-positive">
              <p className="emoji-nobutton">👍</p>
              <p className="count">{notice.positiveVotes}</p>
            </div>
            <div className="vote-negative">
              <p className="emoji-nobutton">👎</p>
              <p className="count">{notice.negativeVotes}</p>
            </div>
          </div>
        }

        {user?.id === notice.user_id && (
          <div className="delete-button">
            <button
              className="delete-new"
              onClick={(e) => {
                e.preventDefault();
                deleteNew(notice.id);
              }}
            >
              Delete
            </button>
          </div>
        )}
      </div>
    </div>
  );
}

export default CardNew;
