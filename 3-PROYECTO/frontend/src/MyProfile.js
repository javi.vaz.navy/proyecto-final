import "./MyProfile.css";
import { useSetUser, useUser } from "./UserContext";
import { Link } from "react-router-dom";

function MyProfile() {
  const user = useUser();
  const setUser = useSetUser();

  const handleSesion = (e) => {
    e.preventDefault();
    setUser("");
  };

  return (
    <div className="my-profile">
      <h1 className="username">{user.userName}</h1>
      <div className="user">
        {user.avatar ? (
          <img
            className="avatar"
            src={`${process.env.REACT_APP_BACKEND}/images/${user.avatar}`}
            alt="avatar"
          />
        ) : (
          <img
            className="without-avatar"
            src={`${process.env.REACT_APP_BACKEND}/images/userOption1.png`}
            alt="avatar"
          />
        )}

        {user.bio ? (
          <div className="bio" dangerouslySetInnerHTML={{ __html: user.bio }} />
        ) : (
          <p>Sin biografía</p>
        )}
      </div>
      <div className="link">
        <Link className="editar" to="/profile/edit">
          editar perfil
        </Link>
        <button onClick={handleSesion} className="cerrar-sesion">
          Cerrar sesión
        </button>
      </div>
    </div>
  );
}

export default MyProfile;
