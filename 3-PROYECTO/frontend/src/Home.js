import CardNew from "./CardNew";
import useFetch from "./useFetch";
import { useUser } from "./UserContext";

function Home() {
  const user = useUser();
  const [notices, setNotices] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/news`
  );
  const [categories] = useFetch(
    `${process.env.REACT_APP_BACKEND}/api/categories`
  );

  const voteNew = async (id, value) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}/api/news/${id}/valoration`,
        {
          method: "POST",
          body: JSON.stringify({ value }),
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + user.token,
          },
        }
      );

      const data = await res.json();

      if (res.ok) {
        setNotices(
          notices.map((n) => {
            if (n.id === id) {
              console.log(data);
              return data;
            }

            return n;
          })
        );
      } else {
        throw new Error("Error votando noticia");
      }
    } catch (error) {
      console.error(error);
    }
  };

  if (!notices || !categories) return <p className="cargando">Cargando...</p>;

  return (
    <div className="home">
      {notices.map((e) => (
        <CardNew
          key={e.id}
          notice={e}
          categories={categories}
          voteNew={voteNew}
          user={user}
        />
      ))}
    </div>
  );
}

export default Home;
