import { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import { useSetUser, useUser } from "./UserContext";
import "./Signin.css";

function Signin() {
  const [email, setEmail] = useState("");
  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPass] = useState("");
  const [error, setError] = useState(null);
  const user = useUser();
  const setUser = useSetUser();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const ret = await fetch("http://localhost:3100/api/users", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ userName, password, email, repeatPassword }),
    });
    const data = await ret.json();
    if (ret.ok) {
      const resLog = await fetch("http://localhost:3100/api/users/login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ email, password }),
      });

      const dataLog = await resLog.json();
      setUser(dataLog);
    } else {
      setError(data.error);
    }
  };

  if (user) return <Redirect to="/" />;

  return (
    <div className="signin">
      <div className="signin-main">
        <h1>Signup</h1>
        <form className="signin-form" onSubmit={handleSubmit}>
          {error && <div className="error">{error + "!!"}</div>}
          <label className="label">
            <input
              className="email"
              name="email"
              placeholder="Email..."
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </label>

          <label className="label">
            <input
              className="username"
              name="userName"
              placeholder="Username..."
              value={userName}
              onChange={(e) => setUsername(e.target.value)}
              required
            />
          </label>
          <label className="label">
            <input
              className="password"
              name="password"
              type="password"
              placeholder="Password..."
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </label>
          <label className="label">
            <input
              className="repeat-pass"
              name="repeatPassword"
              type="password"
              placeholder="Repeat password.."
              value={repeatPassword}
              onChange={(e) => setRepeatPass(e.target.value)}
              required
            />
          </label>
          <button>Registro</button>
        </form>
        <p>
          <span>Ya tienes cuenta?</span>
          <Link className="link" to="/login">
            Inicia sesión
          </Link>
        </p>
      </div>
    </div>
  );
}

export default Signin;
