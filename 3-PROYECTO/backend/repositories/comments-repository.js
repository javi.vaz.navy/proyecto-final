const { database } = require("../infrastructures");

async function getCommentById(id) {
  const query = "SELECT * FROM comments WHERE id=?";
  const [comment] = await database.pool.query(query, id);
  return comment[0];
}

async function getCommentsFromNew(id) {
  const query = "SELECT * FROM comments WHERE new_id=?";
  const [comments] = await database.pool.query(query, id);
  return comments[0];
}

async function createComment(data) {
  const query =
    "INSERT INTO comments (user_id, new_id, text, create_date) VALUES (?, ?, ?, ?)";
  const [comment] = await database.pool.query(query, [
    data.userId,
    data.newId,
    data.text,
    data.createDate,
  ]);
  return comment[0];
}

async function deleteComment(id) {
  const query = "DELETE FROM comments WHERE id=?";
  return await database.pool.query(query, id);
}

module.exports = {
  getCommentsFromNew,
  createComment,
  deleteComment,
  getCommentById,
};
