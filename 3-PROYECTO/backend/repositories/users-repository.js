const { database } = require("../infrastructures");

async function getUserByEmail(email) {
  const query = "SELECT * FROM users WHERE email=?";
  const [users] = await database.pool.query(query, email);
  return users[0];
}

async function getUserById(id) {
  const query = "SELECT * FROM users WHERE id=?";
  const [users] = await database.pool.query(query, id);
  return users[0];
}

async function getUserByUserName(userName) {
  const query = "SELECT * FROM users WHERE user_name = ?";
  const [users] = await database.pool.query(query, userName);
  return users[0];
}

async function createUser({ userName, email, password, signDate }) {
  const insertQuery =
    "INSERT INTO users (user_name, email, password, signin_date) VALUES (?, ?, ?, ?)";
  await database.pool.query(insertQuery, [userName, email, password, signDate]);

  return getUserByEmail(email);
}

async function updateUser(data) {
  const query = "UPDATE users SET bio=?, avatar=?, edit_date=? WHERE id=?";
  await database.pool.query(query, [
    data.bio,
    data.avatar,
    data.editTime,
    data.id,
  ]);
  return getUserById(data.id);
}

async function updateUserPassword(hash, id) {
  const query = "UPDATE users SET password=? WHERE id=?";
  await database.pool.query(query, hash, id);
  return getUserById(id);
}

async function deleteUser(id) {
  const query = "DELETE FROM users WHERE id=?";
  return await database.pool.query(query, id);
}

module.exports = {
  getUserByEmail,
  createUser,
  getUserById,
  updateUser,
  deleteUser,
  getUserByUserName,
  updateUserPassword,
};
