const { database } = require("../infrastructures");

async function getNews() {
  const query = "SELECT * FROM news";
  const [news] = await database.pool.query(query);
  return news;
}

async function findNewById(id) {
  const query = "SELECT * FROM news WHERE id=?";
  const [noticia] = await database.pool.query(query, id);
  return noticia[0];
}

async function createNew(data) {
  const query =
    "INSERT INTO news (user_id, title, text, categorie_id, create_date) VALUES (?, ?, ?, ?, ?)";
  const [noticia] = await database.pool.query(query, [
    data.userId,
    data.titulo,
    data.texto,
    data.categoria,
    data.fechCreacion,
  ]);
  return findNewById(noticia.insertId);
}

async function addNewImage(url, id) {
  const query = "UPDATE news SET image=? WHERE id=?";
  await database.pool.query(query, [url, id]);
  return findNewById(id);
}

async function updateNew(data) {
  const query =
    "UPDATE news SET title=?, text=?, categorie_id=?, edit_time=? WHERE id=?";
  await database.pool.query(query, [
    data.titulo,
    data.texto,
    data.categoria,
    data.fechEdicion,
    data.id,
  ]);
  return findNewById(data.id);
}

async function deleteNewById(id) {
  const query = "DELETE FROM news WHERE id=?";
  return await database.pool.query(query, id);
}

module.exports = {
  getNews,
  findNewById,
  createNew,
  updateNew,
  deleteNewById,
  addNewImage,
};
