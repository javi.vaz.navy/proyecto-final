const { database } = require("../infrastructures");

async function findValorationByUserId(newId, userId) {
  const query = "SELECT * valoration WHERE new_id=?, user_id=?";
  const [value] = await database.pool.query(query, newId, userId);
  return value[0];
}

async function findValorationById(id) {
  const query = "SELECT * FROM valorations WHERE id=?";
  const [value] = await database.pool.query(query, id);
  return value[0];
}

async function createNewValoration(data) {
  const query =
    "INSERT INTO valoration (new_id, user_id, valoration, create_date) VALUES (?, ?, ?, ?)";
  const [value] = await database.pool.query(query, [
    data.newId,
    data.userId,
    data.value,
    data.createDate,
  ]);
  return value[0];
}

async function updateValoration(data) {
  const query = "UPDATE valoration SET valoration=? WHERE id=?";
  await database.pool.query(query, [data.value, data.id]);
  return findValorationById(data.id);
}

async function deleteValoration(id) {
  const query = "DELETE FROM valoration WHERE id=?";
  return await database.pool.query(query, id);
}

module.exports = {
  createNewValoration,
  updateValoration,
  findValorationById,
  findValorationByUserId,
  deleteValoration,
};
