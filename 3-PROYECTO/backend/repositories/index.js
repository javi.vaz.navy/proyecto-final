const usersRepository = require("./users-repository");
const newsRepository = require("./news-repository");
const commentsRepository = require("./comments-repository");
const valorationRepository = require("./valoration-repository");

module.exports = {
  usersRepository,
  newsRepository,
  commentsRepository,
  valorationRepository,
};
