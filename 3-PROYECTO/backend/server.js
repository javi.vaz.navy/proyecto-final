require("dotenv").config();
const path = require("path");
const fs = require("fs");
const express = require("express");
const { v4: uuidv4 } = require("uuid");
const multer = require("multer");

const {
  usersController,
  newsController,
  commentsController,
  valorationController,
} = require("./controllers");

const { validateAuthorization } = require("./middelwares/index");

const { PORT } = process.env;

const staticPath = path.resolve(__dirname, "static");

const app = express();

app.use(express.json());
app.use(express.static(staticPath));

// CONFIGURACIÓN MULTER

const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      const { id } = req.params;
      const folder = path.join(__dirname, `static/images/`);
      fs.mkdirSync(folder, { recursive: true });

      cb(null, folder);
    },
    filename: function (req, file, cb) {
      cb(null, uuidv4() + path.extname(file.originalname));
    },
  }),
  limits: {
    fileSize: 1024 * 1024, // 1 MB
  },
});

// USUARIOS
app.post("/api/users", usersController.registerUser);
app.post("/api/users/login", usersController.loginUser);
app.get("/api/users/:id", usersController.findUserById);
app.patch("/api/users/:id", validateAuthorization, usersController.editUser);
app.post("/api/users/recover-pswd", usersController.recoverPassword);
app.delete("/api/users/:id", validateAuthorization, usersController.deleteUser);

// NOTICIAS
app.get("/api/news", newsController.getNews);
app.get("/api/news/:id", newsController.getNewById);
app.post("/api/news", validateAuthorization, newsController.createNew);
app.patch("/api/news/:id", validateAuthorization, newsController.editNew);
app.patch(
  "/api/news/:id/image",
  validateAuthorization,
  upload.single("image"),
  newsController.addImageIntoNew
);
app.delete("/api/news/:id", validateAuthorization, newsController.deleteNew);

// COMENTARIOS
app.get("/api/news/:id/comment", commentsController.getComments);
app.post(
  "/api/news/:id/comment",
  validateAuthorization,
  commentsController.createNewComment
);
app.delete(
  "/api/new/:id/commment/:id_comment",
  validateAuthorization,
  commentsController.deleteComment
);

// VALORACIONES
// post /api/news/:id/valoration
app.post(
  "/api/news/:id/valoration",
  validateAuthorization,
  valorationController.valorateNew
);
// patch /api/news/:id/valoration/:id_valoration
app.patch(
  "/api/news/:id/valoration/:id_valoration",
  validateAuthorization,
  valorationController.changeValoration
);
// delete /api/news/:id/valoration/:id_valoration
app.delete(
  "/api/news/:id/valoration/:id_valoration",
  validateAuthorization,
  valorationController.deleteValoration
);

app.use(async (err, req, res, next) => {
  const status = err.isJoi ? 400 : err.code || 500;
  res.status(status);
  res.send({ error: err.message });
});

app.listen(PORT, () =>
  console.log(`finalProyect-api listening at port ${PORT}`)
);
