const Joi = require("joi");

const { newsRepository } = require("../repositories");

async function getNews(req, res, next) {
  try {
    const news = await newsRepository.getNews();
    res.send(news);
  } catch (err) {
    next(err);
  }
}

async function getNewById(req, res, next) {
  try {
    const { id } = req.params;
    const noticia = await newsRepository.findNewById(id);
    if (!noticia) {
      const err = new Error("la noticia no existe");
      err.code = 404;
      throw err;
    }
    res.send(noticia);
  } catch (err) {
    next(err);
  }
}

async function createNew(req, res, next) {
  try {
    const { titulo, categoria, texto } = req.body;
    const schema = Joi.object({
      titulo: Joi.string().min(3).max(25).required(),
      categoria: Joi.number().max(5).required(),
      texto: Joi.string().max(15000).required(),
    });
    await schema.validateAsync({ titulo, categoria, texto });

    const data = {
      titulo,
      categoria,
      texto,
      fechCreacion: new Date(),
      userId: req.auth.id,
    };

    const noticia = await newsRepository.createNew(data);

    res.send(noticia);
  } catch (err) {
    next(err);
  }
}

async function addImageIntoNew(req, res, next) {
  try {
    const { id } = req.params;
    const { file } = req;
    const url = `static/images/${file.filename}`;
    const noticia = await newsRepository.findNewById(id);

    if (!noticia) {
      const err = new Error("la noticia no existe");
      err.status = 404;
      throw err;
    }

    if (noticia.user_id !== req.auth.id) {
      const err = new Error("El usuario no tiene permiso");
      err.status = 403;
      throw err;
    }

    await newsRepository.addNewImage(url, id);

    res.send(newsRepository.findNewById(id));
  } catch (err) {
    next(err);
  }
}

async function editNew(req, res, next) {
  try {
    const { id } = req.params;
    const { categoria, texto, titulo } = req.body;

    const schema = Joi.object({
      categoria: Joi.number().max(5).required(),
      titulo: Joi.string().min(3).max(25),
      texto: Joi.string().max(15000),
    });

    await schema.validateAsync({ categoria, texto, titulo });

    const noticia = await newsRepository.findNewById(id);

    if (!noticia) {
      const err = new Error("la noticia no existe");
      err.status = 404;
      throw err;
    }

    if (noticia.user_id !== req.auth.id) {
      const err = new Error("El usuario no tiene permiso");
      err.status = 403;
      throw err;
    }

    const data = {
      id,
      categoria,
      texto,
      titulo,
      fechEdicion: new Date(),
    };
    const updateNoticia = await newsRepository.updateNew(data);

    res.send(updateNoticia);
  } catch (err) {
    next(err);
  }
}

async function deleteNew(req, res, next) {
  try {
    const { id } = req.params;
    const { id: userId } = req.auth;

    const noticia = await newsRepository.findNewById(id);
    if (!noticia) {
      const err = new Error("No existe la noticia");
      err.code = 404;
      throw err;
    }

    if (userId !== noticia.user_id) {
      const err = new Error("El usuario no tiene permiso");
      err.code = 403;
      throw err;
    }

    await newsRepository.deleteNewById(id);

    res.status(204);
    res.send();
  } catch (err) {
    next(err);
  }
}

module.exports = {
  getNews,
  getNewById,
  createNew,
  editNew,
  deleteNew,
  addImageIntoNew,
};
