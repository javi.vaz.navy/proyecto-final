const { valorationRepository } = require("../repositories");
const Joi = require("joi");

async function valorateNew(req, res, next) {
  try {
    const { id: newId } = req.params;
    const { id: userId } = req.auth;
    const { value } = req.body;

    const schema = Joi.number().required();
    await schema.validateAsync(value);

    const valoration = await valorationRepository.findValorationById(
      newId,
      userId
    );

    if (valoration) {
      const err = new Error("El usuario ya ha valorado la noticia");
      err.code = 409;
      throw err;
    }

    const data = {
      newId,
      userId,
      value,
      createDate: new Date(),
    };

    const result = await valorationRepository.createNewValoration(data);

    res.send(result);
  } catch (err) {
    next(err);
  }
}

async function changeValoration(req, res, next) {
  try {
    const { id: newId, id_valoration: valorationId } = req.params;
    const { id: userId } = req.auth;
    const { value } = req.body;

    const schema = Joi.number().required();
    await schema.validateAsync(value);

    const valoration = await valorationRepository.findValorationById(
      newId,
      userId
    );

    if (valoration.user_id !== userId) {
      const err = new Error("El usuario no tiene permiso");
      err.code = 403;
      throw err;
    }

    const data = {
      newId,
      value,
    };

    const result = await valorationRepository.updateValoration(data);

    res.send(result);
  } catch (err) {
    next();
  }
}

async function deleteValoration(req, res, next) {
  try {
    const { id: newId, id_valoration: valorationId } = req.params;
    const { id: userId } = req.auth;

    const valoration = await valorationRepository.findValorationById(
      newId,
      userId
    );

    if (valoration.user_id !== userId) {
      const err = new Error("El usuario no tiene permiso");
      err.code = 403;
      throw err;
    }

    const result = await valorationRepository.deleteValoration(newId);

    res.send(result);
  } catch (err) {
    next(err);
  }
}

module.exports = {
  valorateNew,
  changeValoration,
  deleteValoration,
};
