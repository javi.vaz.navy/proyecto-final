const Joi = require("joi");

const { commentsRepository, newsRepository } = require("../repositories");

async function getComments(req, res, next) {
  try {
    const { id } = req.params;

    const comments = await commentsRepository.getCommentsFromNew(id);
    if (!comments) {
      const err = new Error("La noticia no tiene commentarios");
      err.code = 404;
      throw err;
    }

    res.send(comments);
  } catch (err) {
    next(err);
  }
}

async function createNewComment(req, res, next) {
  try {
    const { text } = req.body;
    const { id: newId } = req.params;

    const schema = Joi.string().min(5).max(255).required();
    await schema.validateAsync(text);

    const notice = newsRepository.findNewById(newId);
    if (!notice) {
      const err = new Error("No se ha encontrado la noticia");
      err.code = 404;
      throw err;
    }

    const data = {
      userId: req.auth.id,
      newId,
      text,
      createDate: new Date(),
    };

    const comment = await commentsRepository.createComment(data);

    res.send(data);
  } catch (err) {
    next(err);
  }
}

async function deleteComment(req, res, next) {
  try {
    const { id: newId, id_comment: commentId } = req.params;
    const { id: userId } = req.auth;

    const notice = await newsRepository.findNewById(newId);
    const comment = await commentsRepository.getCommentById(commentId);

    if (!notice) {
      const err = new Error("No se ha encontrado la noticia");
      err.code = 404;
      throw err;
    }

    if (!comment) {
      const err = new Error("No se ha encontrado el comentario");
      err.code = 404;
      throw err;
    }

    if (userId !== notice.user_id) {
      const err = new Error("el usuario no tiene permiso");
      err.code = 403;
      throw err;
    }

    await commentsRepository.deleteComment(commentId);

    res.status(204);
    res.send();
  } catch (err) {
    next(err);
  }
}

module.exports = {
  getComments,
  createNewComment,
  deleteComment,
};
