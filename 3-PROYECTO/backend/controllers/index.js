const usersController = require("./users-controller");
const newsController = require("./news-controller");
const commentsController = require("./comments-controller");
const valorationController = require("./valoration-controller");

module.exports = {
  usersController,
  newsController,
  commentsController,
  valorationController,
};
