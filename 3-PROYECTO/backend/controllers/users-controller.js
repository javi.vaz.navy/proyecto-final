const Joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const { usersRepository } = require("../repositories");

async function registerUser(req, res, next) {
  try {
    const { userName, email, password, repeatPassword } = req.body;

    const schema = Joi.object({
      userName: Joi.string().min(4).max(15).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(25).required(),
      repeatPassword: Joi.string().min(6).max(25).required(),
    });

    await schema.validateAsync({
      userName,
      email,
      password,
      repeatPassword,
    });

    if (password !== repeatPassword) {
      const err = new Error("Password y repeatPassword no coinciden");
      err.code = 400;
      throw err;
    }

    const user = await usersRepository.getUserByEmail(email);

    if (user) {
      const err = new Error(
        `Ya existe un usuario registrado con el email: ${email}`
      );
      err.code = 409;
      throw err;
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const createdUser = await usersRepository.createUser({
      userName,
      email,
      password: passwordHash,
      signDate: new Date(),
    });

    res.status(201);
    res.send({
      id: createdUser.id,
      userName: createdUser.user_name,
      email: createdUser.email,
    });
    console.log("hola");
  } catch (err) {
    next(err);
  }
}

async function loginUser(req, res, next) {
  try {
    const { email, password } = req.body;

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(25).required(),
    });

    await schema.validateAsync({ email, password });

    const user = await usersRepository.getUserByEmail(email);

    if (!user) {
      const err = new Error(`El usuario con email ${email}, no existe`);
      err.code = 401;
      throw err;
    }

    const isValidPassword = await bcrypt.compare(password, user.password);

    if (!isValidPassword) {
      const err = new Error("La contraseña es incorrecta");
      err.code = 401;
      throw err;
    }

    const tokenPayload = { id: user.id };
    const token = jwt.sign(tokenPayload, process.env.SECRET, {
      expiresIn: "1h",
    });

    res.send({
      id: user.id,
      token,
    });
  } catch (err) {
    next(err);
  }
}

async function findUserById(req, res, next) {
  try {
    const userId = req.params;

    const user = await usersRepository.getUserById(userId);

    if (!user) {
      const err = new Error("El usuario no exixste");
      err.status = 401;
      throw err;
    }

    res.send(user);
  } catch (err) {
    next(err);
  }
}

async function editUser(req, res, next) {
  try {
    const { id } = req.params;
    const { bio, avatar } = req.body;

    const nId = Number(id);

    if (nId !== req.auth.id) {
      const err = new Error("el usuario no tiene permiso");
      err.status = 403;
      throw err;
    }

    const schema = Joi.object({
      bio: Joi.string().max(2555),
      avatar: Joi.string(),
    });

    await schema.validateAsync({ bio, avatar });

    const data = {
      bio,
      avatar,
      id,
      editTime: new Date(),
    };

    const updatedUser = await usersRepository.updateUser(data);

    res.send(updatedUser);
  } catch (err) {
    next(err);
  }
}

async function deleteUser(req, res, next) {
  try {
    const { id } = req.params;

    const user = await usersRepository.getUserById(id);

    if (!user) {
      const err = new Error("No existe el usuario");
      err.status = 404;
      throw err;
    }

    const nId = Number(id);

    if (nId !== req.auth.id) {
      const err = new Error("El usuario no tiene permiso");
      err.code = 403;
      throw err;
    }

    await usersRepository.deleteUser(id);

    res.status(204);
    res.send();
  } catch (err) {
    next(err);
  }
}

async function recoverPassword(req, res, next) {
  try {
    const { userName, email, newPassword, confirmPassword } = req.body;

    const schema = Joi.object({
      userName: Joi.string().min(4).max(15).required(),
      email: Joi.string().email().required(),
      newPassword: Joi.string().min(6).max(25).required(),
      confirmPassword: Joi.string().min(6).max(25).required(),
    });

    await schema.validateAsync({
      userName,
      email,
      newPassword,
      confirmPassword,
    });

    const uName = usersRepository.getUserByUserName(userName);
    const uEmail = usersRepository.getUserByEmail(email);

    if (uName.id !== uEmail.id) {
      const err = new Error("Nombre de usuario o email incorrecto");
      err.code = 401;
      throw err;
    }

    if (!uName || !uEmail) {
      const err = new Error("El usuairo no existe");
      err.code = 404;
      throw err;
    }

    if (newPassword !== confirmPassword) {
      const err = new Error(
        "Nueva contraseña y confirma contraseña no coinciden"
      );
      err.code = 400;
      throw err;
    }

    const passwordHash = await bcrypt.hash(newPassword, 10);

    await usersRepository.updateUserPassword(passwordHash, uName.id);

    res.send(usersRepository.getUserByEmail(uEmail));
  } catch (err) {
    next(err);
  }
}

module.exports = {
  registerUser,
  loginUser,
  findUserById,
  editUser,
  deleteUser,
  recoverPassword,
};
