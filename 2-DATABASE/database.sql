CREATE DATABASE IF NOT EXISTS proyecto_final_backend;

USE proyecto_final_backend;

DROP TABLE `users` IF EXISTS;
DROP TABLE `news` IF EXISTS;
DROP TABLE `comments` IF EXISTS;
DROP TABLE `images` IF EXISTS;
DROP TABLE `valoration` IF EXISTS;

CREATE TABLE IF NOT EXISTS `users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_name` VARCHAR(255) UNIQUE NOT NULL,
    `email` VARCHAR(255) UNIQUE NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `avatar` VARCHAR(255),
    `bio` VARCHAR(525),
    `signin_date` DATETIME NOT NULL,
    `edit_date` DATETIME,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `news` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `title` VARCHAR(55) NOT NULL,
    `text` VARCHAR(15000) NOT NULL,
    `files` VARCHAR(525),
    `categories` VARCHAR(55) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `edit_time` DATETIME,
    PRIMARY KEY (`id`),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `comments` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `new_id` INT NOT NULL,
    `text` VARCHAR(1525),
    `create_date` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (new_id) REFERENCES news(id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS `images` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `new_id` INT NOT NULL,
    `file_name` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (new_id) REFERENCES news(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `valoration` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `new_id` INT NOT NULL,
    `user_id` INT NOT NULL,
    `valoration` INT,
    PRIMARY KEY (`id`),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (new_id) REFERENCES news(id) ON DELETE CASCADE
);